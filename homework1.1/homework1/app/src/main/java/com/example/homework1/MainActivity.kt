package com.example.homework1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    private var userName: EditText? = null
    private var userHeight: EditText? = null
    private var userWeight: EditText? = null
    private var userAge: EditText? = null
    private var button: Button? = null
    private var result: TextView? = null

    private fun checkingForNull(userWeight: EditText?, userHeight: EditText?, userAge: EditText?): Boolean{
        return ((userWeight?.text.toString().toDoubleOrNull() != null) &&
                (userHeight?.text.toString().toIntOrNull() != null) &&
                (userAge?.text.toString().toIntOrNull() != null))
    }
    private fun checkingForUncorrectness(userName: EditText?, userWeight: EditText?, userHeight: EditText?, userAge: EditText?): Boolean{
        return ((userName?.text.toString().length > 50 || userName?.text.toString().equals("")!!) ||
                (userHeight?.text.toString().toInt() < 100 || userHeight?.text.toString().toInt() > 250) ||
                (userWeight?.text.toString().toDouble() < 1 || userWeight?.text.toString().toDouble() > 250) ||
                (userAge?.text.toString().toInt() < 1 || userAge?.text.toString().toInt() > 150))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        userName = findViewById(R.id.userName)
        userHeight = findViewById(R.id.userHeight)
        userWeight = findViewById(R.id.userWeight)
        userAge = findViewById(R.id.userAge)
        button = findViewById(R.id.button)
        result = findViewById(R.id.result)

        button?.setOnClickListener {
            if (checkingForNull(userWeight, userHeight, userAge)) {
                if (checkingForUncorrectness(userName, userWeight, userHeight, userAge))
                    Toast.makeText(this, "Данные введены не корректно!", Toast.LENGTH_LONG).show()
                else {
                    var optWeight = 0.0
                    var optWeightMin = 0.0
                    if (userHeight?.text.toString().toDouble() > 120) {
                        optWeight = (userHeight?.text.toString().toDouble() - 100)
                        optWeightMin = optWeight - 10
                    }
                    else {
                        optWeight = (userHeight?.text.toString().toDouble() - 80)
                        optWeightMin = optWeight - 10
                    }
                    val optCalories = 88.36 + (13.4 * userWeight?.text.toString().toDouble()) +
                    (4.8 * userHeight?.text.toString().toInt()) -
                    (5.7*  userAge?.text.toString().toInt())
                    val pension = userWeight?.text.toString().toDouble() * 30 +
                            userAge?.text.toString().toInt() * 100 + 2000
                    result?.text = "Ваш оптимальный вес составляет: $optWeightMin - $optWeight кг\n" +
                            "Ваша норма калорий в день: $optCalories\n" +
                            "Ваша пенсия будет составлять: $pension\n"
                }
            }
            else {
                Toast.makeText(this, "Сожалеем, но вы неправильно заполнили поля :(", Toast.LENGTH_LONG).show()
            }
        }
    }
}