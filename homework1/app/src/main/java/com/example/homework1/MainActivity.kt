package com.example.homework1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    private var userName: EditText? = null
    private var userHeight: EditText? = null
    private var userWeight: EditText? = null
    private var userAge: EditText? = null
    private var button: Button? = null
    private var result: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        userName = findViewById(R.id.userName)
        userHeight = findViewById(R.id.userHeight)
        userWeight = findViewById(R.id.userWeight)
        userAge = findViewById(R.id.userAge)
        button = findViewById(R.id.button)
        result = findViewById(R.id.result)

        button?.setOnClickListener {
            if ((userName?.text.toString().length > 50 || userName?.text.toString().equals("")!!) ||
                (userHeight?.text.toString().toInt() < 0 || userHeight?.text.toString().toInt() > 250) ||
                (userWeight?.text.toString().toDouble() < 0 || userWeight?.text.toString().toDouble() > 250) ||
                (userAge?.text.toString().toInt() < 0 || userAge?.text.toString().toInt() > 150))
                Toast.makeText(this, "Данные введены не корректно!", Toast.LENGTH_LONG).show()
            else
            {
                var opt = (userHeight?.text.toString().toDouble() - 100)

                result?.text = "Ваш оптимальный вес составляет:$opt кг"
            }
        }
    }
}