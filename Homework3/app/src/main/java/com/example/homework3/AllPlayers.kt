package com.example.homework3

object AllPlayers {
    val players = arrayListOf<Player>(
        Player(0,"Anfernie Simons", 1, "Playmaker", 23, 191,82,"USA"),
        Player(1,"Damian Lillard", 0, "Playmaker", 31, 188,88,"USA"),
        Player(2,"Yusuf Nurkich", 27, "Center", 27, 211,132,"Bosnia and Herzegovina"),
        Player(3,"Eric Bledsou", 12, "Playmaker", 31, 185,97,"USA"),
        Player(4,"Joe Ingles", 21, "Light Forward", 34, 203,100,"Australia"),
        Player(5,"Ben McLemore", 23, "Point Guard", 29, 191,88,"USA"),
        Player(6,"Josh Hart", 11, "Point Guard", 27, 196,98,"USA"),
        Player(7,"Nassir Little", 9, "Light Forward", 22, 196,100,"USA"),
        Player(8,"Brandon Williams", 8, "Playmaker", 22, 188,86,"USA"),
        Player(9,"Justice Winslow", 26, "Light Forward", 26, 198,101,"USA"),
        Player(10,"Trandon Watford", 2, "Heavy Forward", 21, 206,109,"USA"),
        Player(11,"Keon Johnson", 6, "Light Forward", 20, 193,84,"USA")
    )
}