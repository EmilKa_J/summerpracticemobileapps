package com.example.homework3

data class Player(
    val id: Int,
    val name: String,
    val number: Int,
    val position: String,
    val age: Int,
    val height: Int,
    val weight: Int,
    val country: String
)
