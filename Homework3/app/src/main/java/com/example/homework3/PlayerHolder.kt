package com.example.homework3

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.homework3.databinding.ItemPlayerBinding

class PlayerHolder(
    val binding: ItemPlayerBinding,
    private val onItemClick: (Player) -> Unit
) : RecyclerView.ViewHolder(binding.root){

    fun onBind(player: Player) {
        with(binding){
            playerName.text = "#" + player.number + " " +  player.name
            //Добавить номер и другие поля, возможно
            root.setOnClickListener{
                onItemClick(player)
            }
        }
    }
}