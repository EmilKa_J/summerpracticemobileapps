package com.example.homework3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.homework3.databinding.ActivityMainBinding
import com.google.android.material.snackbar.Snackbar

class MainActivity : AppCompatActivity() {

    private var _binding: ActivityMainBinding? = null
    private val binding get() = _binding!!
    private val adapter = PlayerAdapter(AllPlayers.players) {
        Snackbar.make(
            binding.root,
            "Player id: ${it.id}\nNumber: ${it.number}\nPosition: ${it.position}\nAge: ${it.age}\nHeight: ${it.height}\nWeight: ${it.weight}\nCountry: ${it.country}",
            10000
        ).setTextMaxLines(7)
            .setAction("Close", View.OnClickListener {})
            .show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }
//в init прописать логику добавления всех игроков (список уже есть)
    private fun init(){
        binding.apply{
            RCVplayers.layoutManager = LinearLayoutManager(this@MainActivity)
            RCVplayers.adapter = adapter
            adapter.playersList.addAll(AllPlayers.players)
        }
    }
}